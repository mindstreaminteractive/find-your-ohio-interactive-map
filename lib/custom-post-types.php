<?php

namespace Roots\Sage\CustomPostTypes;

// Register Custom Post Type
function custom_post_types()
{
    $args = [
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'exclude_from_search' => true,
        'show_in_nav_menus' => false,
        'has_archive' => false,
        'rewrite' => false,
        'label' => 'People',
        'supports' => ['title', 'editor', 'author', 'thumbnail', 'page-attributes', 'revisions',],
        'rewrite' => [
            'with_front' => false,
        ],
        'menu_icon' => 'dashicons-groups',
    ];
    register_post_type('team', $args);

    register_taxonomy('team-category', 'team', [
        'label' => 'People Categories',
        'hierarchical' => true,
        'publicly_queryable' => false,
        'show_admin_column' => true
    ]);

    $args = [
        'public' => false,
        'label' => 'Carousels',
        'show_ui' => true,
        'exclude_from_search' => true,
        'show_in_nav_menus' => false,
        'has_archive' => false,
        'rewrite' => false,
        'supports' => ['title', 'author', 'revisions',],
        'rewrite' => [
            'with_front' => false,
        ],
        'menu_icon' => 'dashicons-images-alt2',
    ];
    register_post_type('carousel', $args);

    $args = [
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true,
        'exclude_from_search' => true,
        'show_in_nav_menus' => false,
        'has_archive' => false,
        'rewrite' => false,
        'label' => 'Map Regions',
        'supports' => ['title', 'editor', 'author', 'thumbnail', 'page-attributes', 'revisions',],
        'rewrite' => [
            'with_front' => false,
        ],
        'menu_icon' => 'dashicons-admin-site',
    ];
    register_post_type('map-regions', $args);
}

add_action('init', __NAMESPACE__ . '\\custom_post_types', 10);

add_filter('manage_edit-carousel_columns', __NAMESPACE__ . '\\sage_carousel_columns');
function sage_carousel_columns($defaults)
{
    if (array_key_exists('title', $defaults)) {
        $defaults = array_insert_after('title', $defaults, 'shortcode', 'Shortcode');
    } else {
        unset($defaults['date']);
        $defaults['shortcode'] = 'Shortcode';
        $defaults['date'] = 'Publish Date';
    }
    return $defaults;
}

add_action('manage_carousel_posts_custom_column', __NAMESPACE__ . '\\sage_carousel_posts_custom_column', 10, 2);
function sage_carousel_posts_custom_column($column, $post_id)
{
    switch ($column) {
        case 'shortcode': ?>
          <input
            type="text"
            class="fyo-carousel-shortcode-field"
            value='[fyo-carousel id="<?php echo $post_id; ?>"]'
            readonly="true"
            onfocus="this.select();"
            style="width: 250px; border: solid 1px #aaaaaa; font-weight: bold;"
          >
            <?php break;
    }
}

is_admin() && add_action('request', __NAMESPACE__ . '\\sage_carousel_title_orderby', 10);
function sage_carousel_title_orderby($vars)
{
    if ($vars['post_type'] == 'carousel') {
        if (!isset($vars['orderby']) || empty($vars['orderby'])) {
            $vars['orderby'] = 'title';
            $vars['order'] = 'ASC';
        }
    }

    return $vars;
}

/*
 * Inserts a new key/value after the key in the array.
 *
 * @param $key
 *   The key to insert after.
 * @param $array
 *   An array to insert in to.
 * @param $new_key
 *   The key to insert.
 * @param $new_value
 *   An value to insert.
 *
 * @return
 *   The new array if the key exists, FALSE otherwise.
 *
 */
function array_insert_after($key, array &$array, $new_key, $new_value)
{
    if (array_key_exists($key, $array)) {
        $new = [];
        foreach ($array as $k => $value) {
            $new[$k] = $value;
            if ($k === $key) {
                $new[$new_key] = $new_value;
            }
        }
        return $new;
    }
    return false;
}