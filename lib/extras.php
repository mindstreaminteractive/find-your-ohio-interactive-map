<?php

namespace Roots\Sage\Extras;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  if( is_page() && $page_content = get_field('page_content', get_the_ID(), false) ) {
    foreach( $page_content as $k => $section ) {
      if( $k == 0 && $section['acf_fc_layout'] == 'section_start' ) {
        $classes[] = 'no-top-padding';

        switch( $section['field_606b4be9b4b5b'] ) {
          case 'maroon';
          case 'deep-grey';
          case 'red';
          case 'blue';
          case 'teal';
            $classes[] = 'white-color-header';
          break;
        }
      }
    }
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');


//Enable ACF Options Page
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(array(
    'page_title' 	=> __('Theme settings', 'sage'),
    'capability'	=> 'manage_options',
  ));
}

add_action('acf/input/admin_head', __NAMESPACE__ . '\\sage_head_input');
function sage_head_input()
{
  ?>
  <style type="text/css">
    .acf-field.clear-left,
    .acf-field.clear-left[data-width] {
      clear: left !important;
    }

    .acf-field .thumbnail img {
      background: #eee;
      max-width: 100%;
    }

    .acf-flexible-content .layout .acf-fc-layout-handle {
      background: #f2f2f2;
    }

    .acf-field.submenu-text {
      padding-left: 30px !important;
    }

    .acf-field[data-name="section_id"],
    .acf-field[data-name="section_class"] {
      background: #fafafa;
    }

    .link-wrap .link-title {
      display: block;
      padding: 0;
      line-height: 20px;
      font-weight: bold;
    }

    .acf-field.small-height-editor .mce-container iframe,
    .acf-field.small-height-editor .wp-editor-area {
      min-height: 200px !important;
      height: 200px !important;
    }

    .acf-field.tiny-height-editor .mce-container iframe,
    .acf-field.tiny-height-editor .wp-editor-area {
      min-height: 150px !important;
      height: 150px !important;
    }

    .acf-field.left-indent-field {
      padding-left: 50px !important;
    }

    .imagePreview { position:absolute; margin-left: -322px; top:0; left:0; z-index:999999; border:1px solid #f2f2f2; box-shadow:0px 0px 3px #b6b6b6; background-color:#fff; padding:10px;}
    .imagePreview img { width:250px; height:auto; display:block; }
    .acf-tooltip li:hover { background-color:#0074a9; }
  </style>

  <script type="text/javascript">
  (function($){
    String.prototype.replaceAll = function(search, replacement) {
      var target = this;
      return target.replace(new RegExp(search, 'g'), replacement);
    };
    jQuery(document).ready(function($) {
      $(document).on('click', 'a[data-name=add-layout]', function(){
        $('.imagePreview').remove();

        waitForEl('.acf-tooltip li', function() {
          $('.acf-tooltip li a').hover(function(){
            var t = $(this);

            var imageTP = t.attr('data-layout');

            var tooltip_image = $('<div class="imagePreview"><img src="<?php echo get_template_directory_uri(); ?>/templates/flexible-content/images/' + imageTP + '.jpg"></div>');

            tooltip_image.css({
              left: t.offset().left,
              top: t.offset().top
            }).appendTo( $('body') );
          }, function(){
            $('.imagePreview').remove();
          });

          $('.acf-tooltip li a').on('click', function() {
            $('.imagePreview').remove();
          });
        });
      });

      var waitForEl = function(selector, callback) {
        if (jQuery(selector).length) {
          callback();
        } else {
          setTimeout(function() {
            waitForEl(selector, callback);
          }, 100);
        }
      };
    });
  })(jQuery);
  </script>
  <?php
}


function sage_mce4_options($init) {
  $custom_colours = '
        "000000", "Black",
        "ffffff", "White",
        "F20018", "Red",
        "DC182B", "Dark Red",
        "700017", "Maroon",
        "65C8CA", "Blue",
        "525051", "Deep Grey",
        "A1A1A1", "Grey"
    ';

  // build colour grid default+custom colors
  $init['textcolor_map'] = '[' . $custom_colours . ']';

  // change the number of rows in the grid if the number of colors changes
  // 8 swatches per row
  $init['textcolor_rows'] = 2;

  return $init;
}
add_filter('tiny_mce_before_init', __NAMESPACE__ . '\\sage_mce4_options');


add_filter( 'acf/fields/wysiwyg/toolbars' , __NAMESPACE__ . '\\sage_toolbars'  );
function sage_toolbars( $toolbars ) {
  if(in_array('strikethrough', $toolbars['Basic'][1])) {
    unset( $toolbars['Basic'][1][ array_search('strikethrough', $toolbars['Basic'][1]) ] );
  }

  if(isset($toolbars['Basic'][1]) && !array_search('hr', $toolbars['Basic'][1])) {
    array_unshift($toolbars['Basic'][1], 'hr');
  }

  if(isset($toolbars['Basic'][1]) && !array_search('forecolor', $toolbars['Basic'][1])) {
    array_unshift($toolbars['Basic'][1], 'forecolor');
  }

  if(isset($toolbars['Basic'][1]) && !array_search('formatselect', $toolbars['Basic'][1])) {
    array_unshift($toolbars['Basic'][1], 'formatselect');
  }

  if(isset($toolbars['Basic'][1]) && !array_search('styleselect', $toolbars['Basic'][1])) {
    array_unshift($toolbars['Basic'][1], 'styleselect');
  }

  if(isset($toolbars['Full'][1]) && in_array('ari_cf7_button', $toolbars['Full'][1])) {
    $toolbars['Basic']['1'][] = 'ari_cf7_button';
  }

  return $toolbars;
}


add_shortcode('social_links', __NAMESPACE__ . '\\shortcode_social_links');
add_shortcode('social-links', __NAMESPACE__ . '\\shortcode_social_links');
function shortcode_social_links() {
  ob_start();

  if($social_links = get_field('social_links', 'options')): ?>
  <div class="social-links">
    <?php foreach($social_links as $item): ?>
    <a href="<?php echo $item['link']; ?>" target="_blank" rel="nofollow">
      <span class="fa-stack">
        <i class="fa fa-circle fa-stack-2x"></i>
        <i class="fa fa-<?php echo $item['icon']; ?> fa-stack-1x"></i>
      </span>
      <span class="sr-only"><?php echo $item['icon']; ?></span>
    </a>
    <?php endforeach; ?>
  </div>
  <?php endif;

  return ob_get_clean();
}


add_filter( 'wp_calculate_image_srcset', '__return_false' );


function renderLink( $link, $class = '' ) {
  if($link['target']) {
    $target = ' target="'.$link['target'].'" ';
  }
  else {
    $target = '';
  }

  if($class) {
    $_class = ' class="' . $class . '" ';
  }
  else {
    $_class = '';
  }

  return '<a href="' . $link['url'] . '" '. $_class . $target . '>'.str_replace('[br]', '<br>', $link['title']).'</a>';
}


add_filter( 'gform_display_add_form_button', __NAMESPACE__ . '\\display_form_button_on_custom_page' );
function display_form_button_on_custom_page( $is_post_edit_page ) {
  if( is_admin() ) {
    return true;
  }

  return $is_post_edit_page;
}


function admin_style() {
  wp_enqueue_style('admin-styles', get_template_directory_uri().'/admin.css');
}
add_action('admin_enqueue_scripts', __NAMESPACE__ . '\\admin_style');


function acf_flexible_content_layout_title( $title, $field, $layout, $i ) {
  $title = '<strong>' . $title . '</strong>';
  $image = '<div class="thumbnail"><img src="' . get_bloginfo('template_directory') . '/templates/flexible-content/images/' . $layout['name'] . '.jpg" height="100px" /></div>';

  $title .= $image;

  return $title;
}
add_filter('acf/fields/flexible_content/layout_title', __NAMESPACE__ . '\\acf_flexible_content_layout_title', 10, 4);


add_filter( 'gform_next_button', __NAMESPACE__ . '\\input_to_button', 10, 2 );
add_filter( 'gform_previous_button', __NAMESPACE__ . '\\input_to_button', 10, 2 );
add_filter( 'gform_submit_button', __NAMESPACE__ . '\\input_to_button', 10, 2 );
function input_to_button( $button, $form ) {
  $dom = new \DOMDocument();
  $dom->loadHTML( '<?xml encoding="utf-8" ?>' . $button );
  $input = $dom->getElementsByTagName( 'input' )->item(0);
  $new_button = $dom->createElement( 'button' );
  $new_button->appendChild( $dom->createTextNode( $input->getAttribute( 'value' ) ) );
  $input->removeAttribute( 'value' );
  foreach( $input->attributes as $attribute ) {
      $new_button->setAttribute( $attribute->name, $attribute->value );
  }
  $input->parentNode->replaceChild( $new_button, $input );

  return $dom->saveHtml( $new_button );
}


add_filter( 'gform_ajax_spinner_url', __NAMESPACE__ . '\\spinner_url', 10, 2 );
function spinner_url( $image_src, $form ) {
  return 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
}


function upload_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', __NAMESPACE__ . '\\upload_mime_types');


add_filter( 'tiny_mce_before_init', __NAMESPACE__ . '\\mce_before_init_insert_formats' );
function mce_before_init_insert_formats( $init_array ) {
  $style_formats = [
    [
      'title'         => 'Big Title',
      'block'         => 'h1',
      'classes'       => 'big-title',
    ],
    [
      'title'         => 'Paragraph 1',
      'block'         => 'p',
      'classes'       => 'p1',
    ],
    [
      'title'         => 'Paragraph 2',
      'block'         => 'p',
      'classes'       => 'p2',
    ],
    [
      'title'         => 'Paragraph 3',
      'block'         => 'p',
      'classes'       => 'p3',
    ],
  ];

  $init_array['style_formats'] = json_encode( $style_formats );

  return $init_array;
}

add_filter( 'mce_buttons_2', __NAMESPACE__ . '\\mce_buttons_2' );
function mce_buttons_2( $buttons ) {
  array_unshift( $buttons, 'styleselect' );
  return $buttons;
}


add_filter('acf/load_field/name=custom_color', __NAMESPACE__ . '\\field_custom_color');
add_filter('acf/load_field/name=text_color', __NAMESPACE__ . '\\field_custom_color');
add_filter('acf/load_field/name=background_color', __NAMESPACE__ . '\\field_custom_color');
function field_custom_color($field) {
  $screen = null;

  if(function_exists('get_current_screen')) {
    $screen = get_current_screen();
  }

  $field['choices'] = [];

  if(isset($screen->id) && ($screen->id == "acf-field-group")) {
    return $field;
  }

  $choices = [
    'white'             => 'White',
    'red'               => 'Red',
    'dark-red'          => 'Dark Red',
    'grey'              => 'Grey',
    'deep-grey'         => 'Deep Grey',
    'maroon'            => 'Maroon',
    'blue'              => 'Blue',
    'teal'              => 'Teal',
  ];

  $field['choices'] = $choices;

  return $field;
}

/* Dynamic population of slideshow selection on flexible content */
add_filter('acf/load_field/name=slideshow_select', __NAMESPACE__ . '\\field_slideshow_select');
function field_slideshow_select($field) {
  $screen = null;

  if(function_exists('get_current_screen')) {
    $screen = get_current_screen();
  }

  $field['choices'] = [];

  if(isset($screen->id) && ($screen->id == "acf-field-group")) {
    return $field;
  }

  $choices = [];

  $carousels = get_posts([
    'post_type'         => 'carousel',
    'posts_per_page'    => -1,
    'orderby'           => 'title',
    'order'             => 'ASC',
  ]);

  if( $carousels ) {
    foreach( $carousels as $carousel ) {
      $choices[ $carousel->ID ] = get_the_title( $carousel );
    }
  }

  $field['choices'] = $choices;

  return $field;
}


function embed_wrap($cache) {
  return '<div class="embed-responsive embed-responsive-16by9">' . $cache . '</div>';
}


function renderCarousel( $carousel, $atts = [] ) {
  if( $carousel && $carousel_slides = get_field('carousel_slides', $carousel) ):
    $carousel_settings = get_field('carousel_settings', $carousel);
    // $carousel_settings['slides_to_show'] = 2;
    // print "<pre>"; print_r( $carousel_settings ); print "<hr>"; exit;

    $carousel_atts = 'data-slides-count="' . esc_attr( count( $carousel_slides ) ) . '"';

    if( isset( $atts['disable_js'] ) && $atts['disable_js'] ) {
      $carousel_atts .= ' data-disable-js="true"';
    }

    if( isset( $atts['animated_slides'] ) && $atts['animated_slides'] ) {
      $carousel_atts .= ' data-animated-slides="true"';
    }

    if( isset( $carousel_settings['slides_to_show'] ) && $carousel_settings['slides_to_show'] ) {
      $carousel_atts .= ' data-slides-to-show="' . esc_attr( $carousel_settings['slides_to_show'] ) . '"';
    }

    if( isset( $carousel_settings['autoplay_carousel'] ) && $carousel_settings['autoplay_carousel'] ) {
      $carousel_atts .= ' data-autoplay-speed="' . esc_attr( $carousel_settings['autoplay_speed'] ) . '"';
    }

    if( isset( $carousel_settings['transition_mode'] ) && $carousel_settings['transition_mode'] ) {
      $carousel_atts .= ' data-transition-mode="' . esc_attr( sanitize_title( $carousel_settings['transition_mode'] ) ) . '"';
    }

    if( isset( $carousel_settings['transition_direction'] ) && $carousel_settings['transition_direction'] ) {
      $carousel_atts .= ' data-transition-direction="' . esc_attr( sanitize_title( $carousel_settings['transition_direction'] ) ) . '"';
    }
  ?>
  <div class="custom-carousel" <?php echo $carousel_atts; ?>>
    <?php foreach( $carousel_slides as $slide ):
      echo renderCarouselSlide( $slide );
    endforeach; ?>
  </div>
  <?php endif;
}


function renderCarouselSlide( $slide ) {
  ob_start(); ?>
<div class="item-slide" data-mode="<?php echo strtolower( $slide['slide_mode'] ); ?>">
  <div class="slide-wrapper">
    <div>
      <?php foreach( [$slide['1st_part'], $slide['2nd_part']] as $p => $part ):
        $background_image = '';

        if( $slide['slide_mode'] && isset( $part['background_image_' . strtolower( $slide['slide_mode'] )] ) && !empty( $part['background_image_' . strtolower( $slide['slide_mode'] )] ) ):
          $background_image = "background-image: url('" . $part['background_image_' . strtolower( $slide['slide_mode'] )]['sizes']['carousel-' . strtolower( $slide['slide_mode'] )] . "')";
          $mobile_background_image = $part['background_image_' . strtolower( $slide['slide_mode'] )];
        endif;
      ?>
      <div class="slide-part <?php echo $part['flip_background'] ? 'flip-background' : ''; ?> <?php echo $background_image ? 'with-image' : 'without-image'; ?>">
        <?php foreach( ['first', 'second'] as $q => $qurter ): ?>
        <div class="<?php echo $qurter; ?>-quarter part-quarter">
          <div class="bg-color-<?php echo $part['background_color'] ?>" style="<?php echo $background_image; ?>"></div>
        </div>
        <?php endforeach; ?>
        <?php if( $part['text'] ): ?>
        <div class="part-text" >
          <div class="bg-color-<?php echo $part['background_color'] ?>"><?php echo $part['text']; ?></div>
        </div>
        <?php endif; ?>
      </div>
      <?php endforeach; ?>

      <?php if( isset( $mobile_background_image ) && !empty( $mobile_background_image ) ): ?>
      <div class="background-image" style="background-image: url('<?php echo $mobile_background_image['sizes']['buckets']; ?>');"></div>
      <?php endif; ?>
    </div>
  </div>
</div>
  <?php return ob_get_clean();
}


add_shortcode('w', __NAMESPACE__ . '\\shortcode_w', 10, 2);
function shortcode_w( $atts, $content ) {
  return '<span class="text-color-white">' . $content . '</span>';
}


add_shortcode('year', __NAMESPACE__ . '\\shortcode_year');
function shortcode_year( $atts, $content ) {
  return date('Y');
}


add_shortcode('fyo-carousel', __NAMESPACE__ . '\\shortcode_fyo_carousel');
function shortcode_fyo_carousel( $atts ) {
  ob_start();

  if( isset( $atts['id'] ) && !empty( $atts['id'] ) ) {
    renderCarousel( $atts['id'], ['animated_slides' => true] );
  }

  return ob_get_clean();
}

/* Hide the annoying WP news widget from admin */
function remove_dashboard_widgets() {
  global $wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
}
add_action('wp_dashboard_setup', __NAMESPACE__ . '\\remove_dashboard_widgets' );
