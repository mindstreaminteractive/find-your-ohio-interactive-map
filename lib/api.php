<?php

add_action('rest_api_init', function () {
    register_rest_route('api', '/region_collection_request/', [
        'methods' => 'GET',
        'callback' => 'region_json_response'
    ]);
});

function region_json_response(WP_REST_Request $request)
{
    $response = ['results' => []];
    $query = new WP_Query(
        [
            'post_type' => 'map-regions',
        ]
    );
    if ($query->have_posts()) {
        $post_count = 0;
        $data = [];
        while ($query->have_posts()) {
            $query->the_post();
            //region id
            $data[$post_count]['id'] = $post_count;
            $data[$post_count]['region_name'] = get_the_title();
            $data[$post_count]['desktop_background_image'] = get_field('desktop_background_image')['url'];
            $data[$post_count]['mobile_background_image'] = get_field('mobile_background_image')['url'];
            $data[$post_count]['headline_icon'] = get_field('headline_icon');
            $data[$post_count]['headline_text'] = get_field('headline_text');
            $data[$post_count]['body_content'] = get_field('body_content');
            $data[$post_count]['button_text'] = get_field('button_text');
            $data[$post_count]['button_link'] = get_field('button_link');
            $items = get_field('bullets');

            if (is_array($items) && !empty($items)) {
                $data[$post_count]['bullets'] = array_map(function ($item) {
                    return [
                        'headline' => $item['headline'],
                        'content' => $item['content'],
                        'desktop_top' => $item['desktop_top'],
                        'desktop_left' => $item['desktop_left'],
                        'desktop_height' => $item['desktop_height'],
                        'mobile_top' => $item['mobile_top'],
                        'mobile_left' => $item['mobile_left'],
                        'mobile_height' => $item['mobile_height'],
                    ];
                }, $items);
            }
            //bullet height
            $items = get_field('carousel');

            if (is_array($items) && !empty($items)) {
                $data[$post_count]['carousel'] = array_map(function ($item) {
                    return [
                        'image' => $item['image']['url'],
                        'alt' => $item['image']['alt'],
                        'headline' => $item['headline'],
                        'link' => $item['link'],
                        'title' => $item['title'],
                    ];
                }, $items);
            }
            $post_count++;
        }
        $response['results'] = $data;
        return $response;
    }
}

add_action('rest_api_init', function () {
    register_rest_route('api', '/region_categories_request/', [
        'methods' => 'GET',
        'callback' => 'region_categories_json_response'
    ]);
});

function region_categories_json_response(WP_REST_Request $request)
{
    if (empty($request->get_params()) || !array_key_exists('region', $request->get_params())) {
        return ['error' => 'No region set.'];
    }
    $response = ['results' => []];
    if (urldecode($request->get_params()['region']) === 'Southeast Ohio') {
        $region = 'Southeast';
    } else {
        $region = $request->get_params()['region'];
    }
    $query = new WP_Query(
        [
            'post_type' => 'map-regions',
            's' => $region,
        ]
    );
    if ($query->have_posts() && count($query->posts) === 1 && is_a($query->posts[0], WP_Post::class)) {
        $data = [];
        $post = $query->posts[0];
        $data['region_name'] = $post->post_title;
        $data['headline'] = get_field('categories_headline', $post->ID);
        $data['headlineUnderline'] = get_field('categories_headline_underline', $post->ID);
        $categories = (array)get_field('categories', $post->ID);
        if (isset($categories) && !empty($categories)) {
            $data['categories'] = array_map(static function ($category) {
                $locations = $category['locations'];
                if (isset($locations) && !empty($locations)) {
                    $locations = array_map(static function ($location) {
                        return [
                            'buttonImage' => $location['button_image'],
                            'top' => $location['top'],
                            'left' => $location['left'],
                            'locationName' => $location['name'],
                            'locationTitle' => $location['title'],
                            'locationContent' => $location['content'],
                            'locationLink' => $location['link'],
                            'locationMapUrl' => $location['map'],
                            'locationImage' => $location['image'],
                            'locationMobileImage' => $location['mobile_image'],
                        ];
                    }, $locations);
                }
                return [
                    'categoryName' => $category['name'],
                    'categoryImage' => $category['image'],
                    'categoryImageMobile' => $category['image_mobile'],
                    'categoryDefaultContent' => $category['default_content'],
                    'categoryDefaultTitle' => $category['default_title'],
                    'categoryDefaultMap' => $category['default_map'],
                    'locations' => $locations
                ];
            }, $categories);
        }
        $response['results'] = $data;
        return $response;
    }
    var_dump($query->have_posts(), $request->get_params()['region'], count($query->posts), $query->posts);
}