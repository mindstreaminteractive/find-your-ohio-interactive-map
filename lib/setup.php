<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;

define('__GOOGLE_FONTS', ['https://fonts.googleapis.com/css2?family=Spartan:wght@500;600;700&display=swap', 'https://fonts.googleapis.com/css2?family=Caveat&display=swap']);

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  //add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  add_image_size('carousel-vertical', 654, 1308, true);
  add_image_size('carousel-horizontal', 1308, 654, true);
  add_image_size('buckets', 600, 600, true);

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style( __GOOGLE_FONTS[0] );
  add_editor_style( __GOOGLE_FONTS[1] );
  add_editor_style(Assets\asset_path('styles/main.css').'?rand-ver='.filemtime( Assets\asset_disk_path('styles/main.css') ));
}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Primary', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-custom.php'),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {
  wp_enqueue_style('sage/google-fonts-1', __GOOGLE_FONTS[0], [], null);
  wp_enqueue_style('sage/google-fonts-2', __GOOGLE_FONTS[1], [], null);

  /* Interactive Map Enqueue/Register */

  wp_register_script('imap/js', get_template_directory_uri() . '/interactive-map/assets/scripts/toolkit.js', ['jquery', 'sage/js'], [], true);

  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, []); //added ?ver to filename to avoid caching on development
  wp_enqueue_style('imap-styles', get_template_directory_uri() . '/interactive-map/assets/styles/toolkit.css', array(), [], false);
  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }

  wp_register_script('sage/youtube-api', 'https://www.youtube.com/iframe_api', [], null);

  wp_register_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], [], true);

  wp_localize_script('sage/js', 'js_vars', array(
    'ajaxurl'                           => admin_url('admin-ajax.php'),
    'theme_url'                         => get_template_directory_uri(),
  ));

  wp_enqueue_script('sage/js');
  wp_enqueue_script('imap/js'); /* Interactive Map Enqueue */
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);
