<div id="regional-viewer">
  <regional-viewer
    :data-url="'./wp-json/api/region_categories_request?region=<?=get_the_title() ?>'"
    inline-template
  >
    <div class="region-panels">
      <div :class="{opened: showCategory}" class="region-panels-wrap">
        <div class="region-panels__heading">
          <h2
            id="region-headline"
            class="h h-lg animated-headline"
            v-html="headline"
          >
          </h2>
        </div>
        <div class="region-panels">
          <div
            v-for="(category, index) in regionData.categories"
            :key="'category_' + index"
            :tabindex="showCategory ? -1: 0 "
            class="region-panel"
            role="button"
            @click="selectCategory($event, index)"
            @keyup.enter="selectCategory($event, index)"
          >
            <img
              :src="category.categoryImage"
              class="u-hidden u-visible@lg"
            />
            <img
              :src="category.categoryImageMobile"
              class=" u-hidden@lg"
            />
            <div>
              <button class="plus-button" role="none" tabindex="-1">
                <span></span>
                <span></span>
              </button>              <div>
                <strong>
                  <span v-html="category.categoryName"></span>
                </strong>
              </div>
            </div>
          </div>
        </div>
      </div>
      <region-category-view
        :category="currentCategory"
        :close-category="closeCategory"
        :opened="showCategory"
        inline-template
      >
        <div :class="{opened: opened}" class="region-category-wrapper">
          <div :class="{opened: opened}" class="region-category">
            <div :class="{hasImage: hasImage}" :style="locationBackground" class="region-category__content">
              <div>
                <a
                  v-show="!mobileOpen"
                  :ref="'categoryBack'"
                  :class="{disabled: mobileOpen}"
                  class="u-mrg-btm region-category-back"
                  href="#"
                  role="button"
                  tabindex="0"
                  @click="closedCategory($event)"
                  @keyup.enter="closedCategory($event)"
                >
                  <svg  viewBox="0 0 7.091 12.005">
                    <g transform="translate(107.789) rotate(90)">
                      <g  transform="translate(0 100.698)">
                        <path   d="M11.813,101.291l-.393-.4a.658.658,0,0,0-.929,0l-4.486,4.486L1.514,100.89a.658.658,0,0,0-.929,0l-.393.394a.657.657,0,0,0,0,.929l5.347,5.366a.671.671,0,0,0,.466.211h0a.671.671,0,0,0,.464-.211l5.342-5.352a.663.663,0,0,0,.192-.469A.657.657,0,0,0,11.813,101.291Z" transform="translate(0 -100.698)" fill="currentColor"/>
                      </g>
                    </g>
                  </svg>                  <strong>Categories</strong>
                </a>
                <a
                  v-show="mobileOpen"
                  :ref="'locationBack'"
                  :class="{disabled: !mobileOpen, 'u-hidden': !openLocation}"
                  class="u-mrg-btm region-category-back"
                  href="#"
                  role="button"
                  tabindex="0"
                  @click="closedLocation($event)"
                  @keyup.enter="closedLocation($event)"
                >
                  <svg  viewBox="0 0 7.091 12.005">
                    <g transform="translate(107.789) rotate(90)">
                      <g  transform="translate(0 100.698)">
                        <path   d="M11.813,101.291l-.393-.4a.658.658,0,0,0-.929,0l-4.486,4.486L1.514,100.89a.658.658,0,0,0-.929,0l-.393.394a.657.657,0,0,0,0,.929l5.347,5.366a.671.671,0,0,0,.466.211h0a.671.671,0,0,0,.464-.211l5.342-5.352a.663.663,0,0,0,.192-.469A.657.657,0,0,0,11.813,101.291Z" transform="translate(0 -100.698)" fill="currentColor"/>
                      </g>
                    </g>
                  </svg>                  <strong>Back</strong>
                </a>
                <div v-show="currentRegionLocation.locationMobileImage" class="u-visible u-hidden@lg u-mrg-btm">
                  <img :src="currentRegionLocation.locationMobileImage" alt="" class="u-block w100" />
                </div>
                <div>
                  <div v-if="!showLocation" class="region-category__default"  >
                    <div class="h-category region-category__title" v-html="category.categoryDefaultTitle"></div>
                    <p v-html="category.categoryDefaultContent"></p>
                  </div>
                  <div
                    v-if="showLocation"
                    class="region-category__detail"
                  >
                    <div class="h-category" v-html="currentRegionLocation.locationTitle"></div>
                    <p v-html="currentRegionLocation.locationContent"></p>
                    <div class="u-mrg-top u-flex u-flex-row region-location-links">
                      <div>
                        <a
                          v-if="currentRegionLocation.locationLink"
                          :href="currentRegionLocation.locationLink"
                          target="_blank"
                        >
                          <span>Learn more</span>
                          <svg viewBox="0 0 12.578 12.299">
                            <g transform="translate(0 -2.161)">
                              <path
                                d="M82.3,2.161H78.616a.581.581,0,1,0,0,1.162H80.9L75.324,8.895a.581.581,0,0,0,.822.822l5.572-5.572v2.28a.581.581,0,0,0,1.162,0V2.742A.581.581,0,0,0,82.3,2.161Z"
                                fill="currentColor"
                                transform="translate(-70.302)"
                              />
                              <path
                                d="M9.62,37.4a.581.581,0,0,0-.581.581V43.7H1.162V35.819H7.216a.581.581,0,1,0,0-1.162H.581A.581.581,0,0,0,0,35.238v9.039a.581.581,0,0,0,.581.581H9.62a.581.581,0,0,0,.581-.581V37.985A.581.581,0,0,0,9.62,37.4Z"
                                fill="currentColor"
                                transform="translate(0 -30.398)"
                              />
                            </g>
                          </svg>
                        </a>
                      </div>
                      <div>
                        <a
                          v-if="currentRegionLocation.locationMapUrl"
                          :href="currentRegionLocation.locationMapUrl"
                          target="_blank"
                        >
                          <span>Get directions</span>
                          <svg viewBox="0 0 12.578 12.299">
                            <g transform="translate(0 -2.161)">
                              <path
                                d="M82.3,2.161H78.616a.581.581,0,1,0,0,1.162H80.9L75.324,8.895a.581.581,0,0,0,.822.822l5.572-5.572v2.28a.581.581,0,0,0,1.162,0V2.742A.581.581,0,0,0,82.3,2.161Z"
                                fill="currentColor"
                                transform="translate(-70.302)"
                              />
                              <path
                                d="M9.62,37.4a.581.581,0,0,0-.581.581V43.7H1.162V35.819H7.216a.581.581,0,1,0,0-1.162H.581A.581.581,0,0,0,0,35.238v9.039a.581.581,0,0,0,.581.581H9.62a.581.581,0,0,0,.581-.581V37.985A.581.581,0,0,0,9.62,37.4Z"
                                fill="currentColor"
                                transform="translate(0 -30.398)"
                              />
                            </g>
                          </svg>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div v-if="category.categoryDefaultMap" class="region-category__map">
              <div
                class="u-hidden@lg background"
                :style="{backgroundImage:`url(${category.categoryDefaultMap})`}"
              >
                &nbsp;
              </div>
              <div
                class="u-hidden u-visible@lg background"
                :style="{backgroundImage:`url(${category.categoryDefaultMap})`}"
              >
                &nbsp;
              </div>
              <div class="category-location-wrap">
                <div
                  v-for="(location, index) in category.locations"
                  :key="'category_view_' + index"
                  :class="{active: currentRegionLocation.locationName === location.locationName}"
                  :style="{
                  top: location.top,
                  left: location.left,
                  backgroundImage: 'url('+location.buttonImage+')'
                }"
                  class="category-location"
                  role="button"
                  tabindex="0"
                  @click="locationSelected($event, location)"
                  @keyup.enter="locationSelected($event, location)"
                >
                  {{ location.locationName }}
                </div>
              </div>
            </div>
          </div>
        </div>
      </region-category-view>
    </div>
  </regional-viewer>
</div>